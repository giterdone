#!/bin/sh

VERSION=`git tag | tail -n 1 | tr -d 'v'`
echo "Packaging giterdone version $VERSION"
mkdir package
mkdir package/giterdone
mkdir package/giterdone/resources
mkdir package/giterdone/vcs
cp giterdone/resources/*.svg package/giterdone/resources/
cp giterdone/vcs/*.py package/giterdone/vcs/
cp giterdone/*.py package/giterdone/
cp COPYING package/COPYING
cp INSTALL package/INSTALL
cp README package/README
sed "s/\${version}/$VERSION/g" giterdone.gedit-plugin > package/giterdone.gedit-plugin
cd package
zip giterdone giterdone/resources/*.svg \
              giterdone/vcs/*.py \
              giterdone/*.py \
              giterdone.gedit-plugin \
              COPYING \
              INSTALL \
              README
cd ../
mv package/giterdone.zip giterdone-$VERSION.zip
rm -r package
echo "Done"
