# Copyright 2011 Robert Lopez Toscano
#
# This file is part of Giterdone.
#
# Giterdone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# Giterdone is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Giterdone. If not, see <http://www.gnu.org/licenses/>.

import sys
import os.path
common_dir = os.path.join(
    os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
    'giterdone')
sys.path.append(common_dir)

import common
import unittest

class CommonTestCase(unittest.TestCase):

  def test_compare_versions_v1_greater(self):
    v1 = (1,2,10)
    v2 = (1,2,9)
    self.assertTrue(common.compare_versions(v1, v2) > 0)

    v1 = (1,2,10)
    v2 = (0,3,99)
    self.assertTrue(common.compare_versions(v1, v2) > 0)

  def test_compare_versions_v2_greater(self):
    v1 = (1,2,9)
    v2 = (1,2,10)
    self.assertTrue(common.compare_versions(v1, v2) < 0)

    v1 = (0,3,99)
    v2 = (1,2,10)
    self.assertTrue(common.compare_versions(v1, v2) < 0)

  def test_compare_versions_equal(self):
    v1 = (1,1,1)
    v2 = (1,1,1)
    self.assertTrue(common.compare_versions(v1, v2) == 0)

    v1 = (1,0,11)
    v2 = (1,0,11)
    self.assertTrue(common.compare_versions(v1, v2) == 0)

if __name__ == '__main__':
  unittest.main()
